#pragma once
#include <Box2D.h>
#include "World.h"

class Particle
{
	private:
	protected:
		b2Vec2 pos;
		b2Vec2 vel;
		b2Vec2 accel;
		double rpos[2];
    double rvel[2];
    double racc[2];

	public:
		Particle(b2World* w);
		Particle(b2World* w, double velX, double velY, double posX, double posY, float width, float height, float density, float cof, float cor);
		~Particle();
		b2Vec2 getPosition();


	void render();
		b2World* world;
};
