#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include "Particle.h"
#include "World.h"

GLfloat X = 0.0f;        // Translate screen to x direction (left or right)
GLfloat Y = 0.0f;        // Translate screen to y direction (up or down)


World* world = World::getWorld(); //makes world object
Particle* b; //starts particle object
Particle* c;
void HandleTimer(int ID);
float32 timeStep = 1.0f / 200.0f;
int32 velocityIterations = 50;
int32 positionIterations = 50;


void init(void)
{
    	float vel;
	float ang;
	printf ("Enter a velocity for your box in m/s(use decilal point, 1.0, 2.5, etc): ");
  	scanf ("%f", &vel);
	printf ("enter an angel for your box in degrees(use decimal points): ");
	scanf ("%f", &ang);
	float vely = vel*sin(ang*0.0174532925);
	printf ("%f\n", vely);
	float velx = vel*cos(ang*0.0174532925);
	printf ("%f\n", velx);



	world->define(); //defines world object
	b2World* sent = world->sendWorld();
	c = new Particle (sent);//makes particle object, sends world settings to particle
	b = new Particle (sent, velx, vely, 1.0,500.0, 100.0f, 100.0f, 10.0f, 0.6f, 0.3f);//makes particle object, sends world settings to particle


	glViewport(10,10,780,580);//sets displayable area
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (0.0,10000.0, 0.0, 7500.0);   // scales window output
	glutPostRedisplay();

}



void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	world->render();//renders world
	c->render();//renders particle
	b->render();//renders particle

	glutSwapBuffers();//shows buffered image
}

void idle(void)
{
    int time = glutGet(GLUT_ELAPSED_TIME)/200.0f;
world->step(timeStep*time, velocityIterations, positionIterations);//time step
glutPostRedisplay();
}

int main (int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800,600);  // Size of the OpenGL window
	glutCreateWindow("Oreos Test"); // Creates OpenGL Window
	glutDisplayFunc(display);
	glutIdleFunc(idle);
//	glutKeyboardFunc(keyCB);        // set window's key callback
//	glutSpecialFunc(specialKey);    // set window's to specialKey callback
	//glutTimerFunc(3000,HandleTimer,1);/// See HandleTimer(...)


	init();
	glutMainLoop();
	return 0;


}


void HandleTimer(int ID){


	/// This function send a message to the main loop that it's time to ca
	/// the DisplayFunc which I called "RenderScene"
	glutPostRedisplay();
	/// The first value is the number of millseconds before the next event
	/// The second arg is a pointer to the callback function
	/// The third is the ID number of this timer
	glutTimerFunc(16,HandleTimer,1);
}
