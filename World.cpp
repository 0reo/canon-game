#include "World.h"
#include <GL/glut.h>
b2Body* groundBody;
World::World()
{


	worldAABB.lowerBound.Set(-10000.0f, -10000.0f);
	worldAABB.upperBound.Set(10000.0f, 10000.0f);

	// Define the gravity vector.
	gravity.Set(0.0f, -10.0f);

	// Do we want to let bodies sleep?
	doSleep = true;

	// Construct a world object, which will hold and simulate the rigid bodies.
}


World::World(float gravX, float gravY)
{

	worldAABB.lowerBound.Set(-1000.0f, -1000.0f);
	worldAABB.upperBound.Set(1000.0f, 1000.0f);

	// Define the gravity vector.
	gravity.Set(gravX, gravY);

	// Do we want to let bodies sleep?
	doSleep = true;

	// Construct a world object, which will hold and simulate the rigid bodies.
}


void World::define()
{

	world = new b2World(worldAABB, gravity, doSleep); //sets new world
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f,10.0f);

	groundBody = world->CreateBody(&groundBodyDef);

	//makes the ground a box shape
	b2PolygonDef groundShapeDef;
	groundShapeDef.SetAsBox(10000.0f, 20.0f);//takes half width and half height.  so width is 100 and height is 20 (in meters).  mult real meters by 10 to get pixles
		groundShapeDef.restitution = 0.5f;
	groundBody->CreateShape(&groundShapeDef);


}

void World::render()
{
	glColor3f(0.0,0.0,1.0);
	b2Vec2 pos = groundBody->GetPosition();
	glRectf(pos.x,pos.y,pos.x+10000.0,pos.y+20.0);
//printf("%f, %f\n",pos.x, pos.y);

}


World* World::instance=0;

World* World::getWorld()
{
	if (!instance)
		instance = new World();
	return instance;

}

b2World* World::sendWorld()
{
	return world;
}

void World::step(float time, int velocity, int position)
{

	world->Step(time, velocity);
}
