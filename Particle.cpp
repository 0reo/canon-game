#include <stdio.h>
#include "Particle.h"
#include "World.h"
#include <GL/glut.h>

int newSize;
b2Body* body;
b2Body* body2;
Particle::Particle(b2World* w)//sets up big block
{

	world = w;

		vel.Set(-86.8,0.0);
		accel.Set(2.0, 2.0);
		pos.Set(8000.0, 500.0);


		b2BodyDef bodyDef;
		bodyDef.position.Set(pos.x, pos.y);
		body = world->CreateBody(&bodyDef);

		b2PolygonDef shapeDef;
		shapeDef.SetAsBox(500.0f, 500.0f);//makes the particle a box shape, uses half widths and heights
		shapeDef.density = 1.0f;
		shapeDef.friction = 0.0f;
		shapeDef.restitution = 0.6f;
		body->CreateShape(&shapeDef);
		body->SetLinearVelocity(vel);
		bodyDef.allowSleep = true;
		bodyDef.isSleeping = false;
		body->SetMassFromShapes();




}


Particle::Particle(b2World* w, double velX, double velY, double posX, double posY, float width, float height, float density, float cof, float cor)//sets up small block
{

	world = w;

		vel.Set(velX,velY);

		pos.Set(posX, posY);

		b2BodyDef bodyDef2;
		bodyDef2.position.Set(pos.x, pos.y);
		body2 = world->CreateBody(&bodyDef2);

		b2PolygonDef shapeDef2;
		shapeDef2.SetAsBox(width, height);//makes the particle a box shape, uses half widths and heights
		shapeDef2.density = density;
		shapeDef2.friction = cof;
		shapeDef2.restitution = cor;
		body2->CreateShape(&shapeDef2);
		body2->SetLinearVelocity(vel);

		bodyDef2.allowSleep = true;
		bodyDef2.isSleeping = false;
		body2->SetMassFromShapes();




}


void Particle :: render()//draws particles
{

	glColor3f(1.0,1.0,0.0);

	b2Vec2 position = body->GetPosition();
	b2Vec2 position2 = body2->GetPosition();

	float32 angle = body->GetAngle();
//	glut
	glRectf(position.x,position.y,position.x-500.0,position.y-500.0);
	glRectf(position2.x,position2.y,position2.x+100.0,position2.y-100.0);
	//printf("%f\n",position.x);


}

