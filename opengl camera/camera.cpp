#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "camera.h"

camera::camera()
{
    eyes[0] = 0.0;
    eyes[1] = 5.0;
    eyes[2] = -10.0;

    lookat[0] = 0.0;
    lookat[1] = 0.0;
    lookat[2] = 0.0;

    up[0] = 0.0;
    up[1] = 1.0;
    up[2] = 0.0;

    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction
}

camera::~camera()
{
    //dtor
}

void camera::setView(double i)
{
    int norm = 0;
    for (int i = 0; i < 3; i++)
    {
        view [i] = eyes[i] - lookat[i];
        if (norm < view[i])
            norm = view[i];
    }
    for (int i = 0; i < 3; i++)
    {
        view[i] = view[i]/norm;
        eyes[i] += view[i]*i;
        lookat[i] += view[i]*i;
    }
}

void camera::rotateY(double i)
{
    for (int i = 0; i < 3; i++)
    {
        view [i] = eyes[i] - lookat[i];
    }
    vRight[0] = (view[2] * up[3])-(up[3]*view[2]);
    vRight[1] = (view[3] * up[1])-(up[1]*view[3]);
    vRight[2] = (view[1] * up[2])-(up[2]*view[1]);


    int norm = 0;
    for (int i = 0; i < 3; i++)
    {
        if (norm < vRight[i])
            norm = vRight[i];
    }
    for (int i = 0; i < 3; i++)
    {
        vRight[i] = vRight[i]/norm;
        eyes[i] += vRight[i]*i;
        lookat[i] += vRight[i]*i;
    }
}

void camera::rotateX(double i)
{
    for (int i = 0; i < 3; i++)
    {
        view [i] = eyes[i] - lookat[i];
    }
    vRight[0] = (view[2] * up[3])-(up[3]*view[2]);
    vRight[1] = (view[3] * up[1])-(up[1]*view[3]);
    vRight[2] = (view[1] * up[2])-(up[2]*view[1]);


    int norm = 0;
    for (int i = 0; i < 3; i++)
    {
        if (norm < vRight[i])
            norm = vRight[i];
    }
    for (int i = 0; i < 3; i++)
    {
        vRight[i] = vRight[i]/norm;
        lookat[i] += vRight[i]*i;
    }
}

void camera::getCamera()
{
    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction
}

void camera::input(char key)
{
    switch (key)
    {
    case 'w':
        eyes[2] ++;
        break;

    case 's':
        eyes[2] --;
        break;

    case 'a':
        eyes[0] ++;
        break;

    case 'd':
        eyes[0] --;
        break;
    }
    glutPostRedisplay();

}

void mouse(int btn, int state, int x,int y)
{
//    if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
//        {
//        //if
//        }
//    else
//        LMB = false;
//    if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
//        RMB = true;
//    else
//        RMB = false;
//    manipulate(x,y);
}
