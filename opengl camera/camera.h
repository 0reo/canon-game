#ifndef CAMERA_H
#define CAMERA_H
#include <GL/glu.h>

typedef struct Vector
{
    double x, y,z;
}_Vector;

class camera
{
    public:
        camera();
        virtual ~camera();
        void setCameraPosition();
        Vector getCameraPosition();

        void setUp();
        Vector getUp();

        void setLookAt();
        Vector getLookAt();

        void setView(double i);
        Vector getView();

        void  rotateX(double i);
        void moveZ();
        void rotateY(double i);

        void getCamera();

        void input(char key);


    protected:
    private:
    float eyes[3], up[3], lookat[3], view[3], vRight[3];
    int oldX, oldY, oldZ;

};

#endif // CAMERA_H
