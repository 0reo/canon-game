#pragma once
#include <Box2D.h>
class World
{
	private:
		World();
		World(float gravX, float gravY);
		~World();
		static World* instance;
	protected:
		b2AABB worldAABB;
		b2Vec2 gravity;
		b2Vec2 pos;
		b2Vec2 vel;
		b2Vec2 accel;
		bool doSleep;
		double rpos[2];
		double rvel[2];
		double racc[2];

	public:
		static World* getWorld(); //initalizes class
		b2World* sendWorld(); //used to send world pointer to particle class/object so said particle can be made using box2d
		void define();//need to finish this;  lets you define world settings like gravity, etc
		virtual	void render();//renders world to screen
		void step(float time, int velocity, int position);
		b2World* world;
		void update (double time);
};
